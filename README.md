TimeLord
========

A simple Personal Software Process (PSP) application that allows a user to log into a JIRA server and retrieve tasks. Then record start, stop, delta time and a description for work completed.

[TimeLord Documentation](https://mullsy.bitbucket.io/timelord/)