package model;

import java.io.Serializable;

/**
 * Container class to hold required parameters of a Jira project issue i.e. 
 * <ul><li>the creation date</li>
 * 	   <li>name and</li>
 * 	   <li>summary of each issue</li>
 * </ul>
 * 
 * @author mullsy
 */
public class Issue implements Serializable
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------
	/**
	 * Issue
	 */
	private static final long serialVersionUID = -4145102550022066530L;
	
	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------
	private String name;
	private String creationDate;
	private String summary;
	
	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------
	public Issue()
	{
		this.name = "";
		this.creationDate = "";
		this.summary = "";
	}

	////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Accessor and Mutator Methods ///////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	public String getName()
	{
		return name;
	}

	public void setName( String name )
	{
		this.name = name;
	}

	public String getCreationDate()
	{
		return creationDate;
	}

	public void setCreationDate( String creationDate )
	{
		this.creationDate = creationDate;
	}

	public String getSummary()
	{
		return summary;
	}

	public void setSummary( String summary )
	{
		this.summary = summary;
	}
}