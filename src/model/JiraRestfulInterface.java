package model;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * This class provides an interface to a JIRA server and allows the retrieval of project and issue
 * data.
 * 
 * The credentials used to log into the server and the project details are retrieved from the
 * preferences file. Currently only issues with 'open' status will be returned.
 * 
 * @author mullsy
 * 
 */
public class JiraRestfulInterface
{
	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------
	private String jiraBaseUrl;
	private String username;
	private String password;


	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------
	public JiraRestfulInterface( String url, String username, String password )
	{
		// Create the parameter variables
		this.jiraBaseUrl = url;
		this.username = username;
		this.password = password;
	}


	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------
	/**
	 * Uses the base Jira URL and supplied path to create a connection to the Jira server
	 * of the type specified.
	 * 
	 * @param urlPath The path that when postfixed to the base Jira URL provides the complete URL for a connection.
	 * @param requestType GET or POST
	 * 
	 * @return HttpURLConnection to the Jira server.
	 * 
	 * @throws IOException
	 */
	private HttpURLConnection connectToJira( String urlPath, String requestType )
	    throws IOException
	{
		URL jiraUrl = new URL( this.jiraBaseUrl + urlPath );
		String login = this.username + ":" + this.password;
		String encodedBytes = Base64.getEncoder().encodeToString( login.getBytes( "utf-8" ) );

		HttpURLConnection connection = (HttpURLConnection)jiraUrl.openConnection();
		connection.setRequestMethod( requestType );
		connection.setRequestProperty( "Accept", "*/*" );
		connection.setRequestProperty( "Content-Type", "application/json" );
		connection.setRequestProperty( "Authorization", "Basic " + encodedBytes );
		connection.setUseCaches( false );
		connection.setDoOutput( true );
		connection.setDoInput( true );
		connection.connect();

		return connection;
	}

	/**
	 * A basic call to the Jira server to see if it is available.
	 * 
	 * @return True if the server can be connected to.
	 */
	public boolean jiraServerAvailable()
	{
		try
		{
			connectToJira( "rest/api/2/serverInfo?true", "GET" );
			return true;
		}
		catch( IOException e )
		{
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Polls the Jira server for all the available projects.  From this, a list of issues per project
	 * can be created.
	 * 
	 * @return List<String> of all projects on the Jira server.
	 * 
	 * @throws IOException
	 */
	public List<String> getAllProjects()
	{
		List<String> allProjects = new ArrayList<String>();

		HttpURLConnection connection;
		try
		{
			connection = connectToJira( "rest/api/2/project", "GET" );
			
			// Read the server response
			BufferedReader inputStreamReader =
			    new BufferedReader( new InputStreamReader( connection.getInputStream(), "UTF-8" ) );

			// Create a JSON array from the server response which holds each project returned.
			String line = inputStreamReader.readLine();
			JSONArray projectsArray = new JSONArray( line );
			
			// Get the name of each project.
			for( int i = 0; i < projectsArray.length(); i++ )
			{
				JSONObject entry = projectsArray.getJSONObject( i );
				Iterator<String> key = entry.keys();

				while( key.hasNext() )
				{
					String current = key.next().toString();
					if( current.equals( "name" ) )
						allProjects.add( (String)entry.get( current ) );
				}
			}
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}

		return allProjects;
	}


	/**
	 * Queries the Jira server for all the issues for the given project.  This list is then used
	 * to populate the Jira drop down menu in the main GUI.
	 * 
	 * @param project The project to get all issues for.
	 * 
	 * @return List of {@link Issue} objects containing the creation date, name and summary of each issue.
	 * 
	 * @throws IOException
	 */
	public List<Issue> getMyIssues( String project )
	    throws IOException
	{
		List<Issue> issues = new ArrayList<Issue>();
		
		HttpURLConnection connection = connectToJira( "rest/api/2/search", "POST" );

		// Create the JQL query
		String data = "{\"jql\":\"project = " + project + "\"}";
		byte[] dataBytes = data.getBytes( "UTF-8" );

		// Write JQL query to output stream
		DataOutputStream wr = new DataOutputStream( connection.getOutputStream() );
		wr.write( dataBytes );
		wr.flush();
		wr.close();

		// Read response
		BufferedReader in = new BufferedReader( new InputStreamReader( connection.getInputStream(), "UTF-8" ) );

		String line = in.readLine();

		JSONObject jsonObject = new JSONObject( line );
		JSONArray issuesArray = jsonObject.getJSONArray( "issues" );

		for( int i = 0; i < issuesArray.length(); i++ )
		{
			JSONObject issueObject = issuesArray.getJSONObject( i );
			Iterator<String> issueKey = issueObject.keys();
			
			// Create Issue objet for the project's issues.
			Issue issue = new Issue();
			while( issueKey.hasNext() )
			{
				String current = issueKey.next().toString();

				if( current.equals("fields") )
				{
					JSONObject issueFields = issueObject.getJSONObject( "fields" );
					issue.setSummary( issueFields.get("summary").toString() );
					issue.setCreationDate( issueFields.get("created").toString() );
				}
				else if( current.equals("key") ) 
					issue.setName( issueObject.get("key").toString() );

			}
			issues.add( issue );
		}
		
		return issues;
	}
}
